<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserController extends Controller
{

    /**
     * @Route("/user/home", name="homeuser")
     */
    public function homeUserAction()
    {
        return $this->render('user/home/index.html.twig');

    }

    /**
     * @Route("/user/login", name="loginuser")
     */
    public function loginUserAction()
    {
        return $this->render('user/login/login.html.twig');

    }
}

