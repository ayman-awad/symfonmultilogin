<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminController extends Controller
{

    /**
     * @Route("/admin/login", name="loginadmin")
     */
    public function LoginAction()
    {
        return $this->render("admin/login/login.html.twig");
    }

    /**
     * @Route("/admin/home", name="homeadmin")
     */
    public function homeAdminAction()
    {
        return $this->render('admin/home/index.html.twig');

    }

}
