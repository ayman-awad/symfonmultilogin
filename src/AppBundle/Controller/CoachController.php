<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CoachController extends Controller
{
    /**
     * @Route("/coach/login", name="logincoach")
     */
    public function LoginAction()
    {
        return $this->render("coach/login/login.html.twig");
    }

    /**
     * @Route("/coach/home", name="homecoach")
     */
    public function homeAdminAction()
    {
        return $this->render('coach/home/home.html.twig');

    }
}
